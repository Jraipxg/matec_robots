#!/usr/bin/env python

import rospy
import roslib; roslib.load_manifest("atlas_moveit_config")
import rospkg

import os

if __name__=="__main__":

    rospy.init_node("srdf_assembler")

    package_name = "atlas_moveit_config"
    try :
        rp = rospkg.RosPack()
        package_path = rp.get_path(package_name)
    except :
        rospy.logerr(str("package " + package_name + " not found"))
        exit()

    left = rospy.get_param("~left", "sri")
    right = rospy.get_param("~right", "robotiq")
    filename = rospy.get_param("~filename", "temp.srdf")

    res_path = package_path + "/resources/"
    base_path = package_path + "/config/"
    base_file = "atlas_base.xml"
    basic_groups_file = "atlas_basic_groups.xml"
    left_hand_file = "atlas_" + str(left) + "_left.xml"
    right_hand_file = "atlas_" + str(right) + "_right.xml"
    collisions_file = "atlas_collisions.xml"
    tail_file = "atlas_tail.xml"

    output_file = package_path + "/config/" + filename
    print output_file

    os.system("rm " + output_file)
    os.system("cat " + res_path + base_file + " >> " + output_file)
    os.system("cat " + res_path + basic_groups_file + " >> " + output_file)
    os.system("cat " + res_path + left_hand_file + " >> " + output_file)
    os.system("cat " + res_path + right_hand_file + " >> " + output_file)
    os.system("cat " + res_path + collisions_file + " >> " + output_file)
    os.system("cat " + res_path + tail_file + " >> " + output_file)
