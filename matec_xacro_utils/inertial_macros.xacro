<?xml version="1.0"?>
<robot name="macros" xmlns:xacro="http://www.ros.org/wiki/xacro">
  <!-- densities and other constants -->
  <xacro:property name="tennis_ball_density" value="400"/> <!-- kg / m^3 -->
  <xacro:property name="southern_yellow_pine" value="840"/> <!-- kg / m^3 -->
  <xacro:property name="lexan_density" value="1190"/> <!-- kg / m^3 -->
  <xacro:property name="pvc_density" value="1300"/> <!-- kg / m^3 -->
  <xacro:property name="aluminum_density" value="2700"/> <!-- kg / m^3 -->
  <xacro:property name="steel_density" value="7900"/> <!-- kg / m^3 -->

  <xacro:property name="pi" value="3.1414926535"/>
  <xacro:property name="gravity" value="9.80665"/> 

  <!-- origin should point to the center of the cylinder -->
  <!-- assumes that the cylinder's axis is aligned to the z axis of the frame -->
  <xacro:macro name="cylinder_inertial" params="density radius height *origin">
    <inertial>
      <mass value="${(density*height*pi*radius*radius)}"/>
      <insert_block name="origin"/>
      <inertia ixx="${(density*height*pi*radius*radius)*(3*radius*radius + height*height)/12.0}" ixy="0.0" ixz="0.0" iyy="${(density*height*pi*radius*radius)*(3*radius*radius + height*height)/12.0}" iyz="0.0" izz="${(density*height*pi*radius*radius)*radius*radius/2.0}"/>
    </inertial>
  </xacro:macro>
  <xacro:macro name="cylinder_limits" params="effort_scale density radius height velocity_limit joint_lower joint_upper">
    <limit effort="${(density*height*pi*radius*radius)*gravity*effort_scale*height}" velocity="${velocity_limit}" lower="${joint_lower}" upper="${joint_upper}"/>
  </xacro:macro>
  <xacro:macro name="cylinder_axis_dynamics" params="density radius height axis_x axis_y axis_z damping_scale friction_scale">
    <axis xyz="${axis_x} ${axis_y} ${axis_z}"/>
    <dynamics damping="${damping_scale*(axis_x*((density*height*pi*radius*radius)*(3*radius*radius + height*height)/12.0)+axis_y*((density*height*pi*radius*radius)*(3*radius*radius + height*height)/12.0)+axis_z*((density*height*pi*radius*radius)*radius*radius/2.0))}" friction="${friction_scale*(axis_x*((density*height*pi*radius*radius)*(3*radius*radius + height*height)/12.0)+axis_y*((density*height*pi*radius*radius)*(3*radius*radius + height*height)/12.0)+axis_z*((density*height*pi*radius*radius)*radius*radius/2.0))}"/>
  </xacro:macro>

  <!-- origin should point to the center of the cylinder -->
  <!-- assumes that the cylinder's axis is aligned to the z axis of the frame -->
  <xacro:macro name="hollow_cylinder_inertial" params="density inner_radius outer_radius height *origin">
    <inertial>
      <mass value="${(density*height*pi*(outer_radius*outer_radius - inner_radius*inner_radius))}"/>
      <insert_block name="origin"/>
      <inertia ixx="${(density*height*pi*(outer_radius*outer_radius - inner_radius*inner_radius))*(3.0*(inner_radius*inner_radius + outer_radius*outer_radius) + height*height)/12.0}" ixy="0.0" ixz="0.0" iyy="${(density*height*pi*(outer_radius*outer_radius - inner_radius*inner_radius))*(3.0*(inner_radius*inner_radius + outer_radius*outer_radius) + height*height)/12.0}" iyz="0.0" izz="${(density*height*pi*(outer_radius*outer_radius - inner_radius*inner_radius))*(inner_radius*inner_radius + outer_radius*outer_radius)/2.0}"/>
    </inertial>
  </xacro:macro>
  <xacro:macro name="hollow_cylinder_limits" params="effort_scale density inner_radius outer_radius height velocity_limit joint_lower joint_upper">
    <limit effort="${(density*height*pi*(outer_radius*outer_radius - inner_radius*inner_radius))*gravity*effort_scale*height}" velocity="${velocity_limit}" lower="${joint_lower}" upper="${joint_upper}"/>
  </xacro:macro>
   <xacro:macro name="hollow_cylinder_axis_dynamics" params="density inner_radius outer_radius height axis_x axis_y axis_z damping_scale friction_scale">
    <axis xyz="${axis_x} ${axis_y} ${axis_z}"/>
    <dynamics damping="${damping_scale*(axis_x*((density*height*pi*(outer_radius*outer_radius - inner_radius*inner_radius))*(3.0*(inner_radius*inner_radius + outer_radius*outer_radius) + height*height)/12.0)+axis_y*((density*height*pi*(outer_radius*outer_radius - inner_radius*inner_radius))*(3.0*(inner_radius*inner_radius + outer_radius*outer_radius) + height*height)/12.0)+axis_z*((density*height*pi*(outer_radius*outer_radius - inner_radius*inner_radius))*(inner_radius*inner_radius + outer_radius*outer_radius)/2.0))}" friction="${friction_scale*(axis_x*((density*height*pi*(outer_radius*outer_radius - inner_radius*inner_radius))*(3.0*(inner_radius*inner_radius + outer_radius*outer_radius) + height*height)/12.0)+axis_y*((density*height*pi*(outer_radius*outer_radius - inner_radius*inner_radius))*(3.0*(inner_radius*inner_radius + outer_radius*outer_radius) + height*height)/12.0)+axis_z*((density*height*pi*(outer_radius*outer_radius - inner_radius*inner_radius))*(inner_radius*inner_radius + outer_radius*outer_radius)/2.0))}"/>
  </xacro:macro>

  <!-- origin should point to the center of the box -->
  <xacro:macro name="box_inertial" params="density size_x size_y size_z *origin">
    <inertial>
      <mass value="${(density*size_x*size_y*size_z)}"/>
      <insert_block name="origin"/>
      <inertia ixx="${(density*size_x*size_y*size_z) / 12 * (size_y * size_y + size_z * size_z)}" ixy="0.0" ixz="0.0" iyy="${(density*size_x*size_y*size_z) / 12 * (size_x * size_x + size_z * size_z)}" iyz="0.0" izz="${(density*size_x*size_y*size_z) / 12 * (size_x * size_x + size_y * size_y)}"/>
    </inertial>
  </xacro:macro>
  <xacro:macro name="box_limits" params="effort_scale density size_x size_y size_z velocity_limit joint_lower joint_upper">
    <limit effort="${(density*size_x*size_y*size_z)*gravity*effort_scale*size_x*size_y*size_z}" velocity="${velocity_limit}" lower="${joint_lower}" upper="${joint_upper}"/>
  </xacro:macro>
   <xacro:macro name="box_axis_dynamics" params="density size_x size_y size_z axis_x axis_y axis_z damping_scale friction_scale">
    <axis xyz="${axis_x} ${axis_y} ${axis_z}"/>
    <dynamics damping="${damping_scale*(axis_x*((density*size_x*size_y*size_z) / 12 * (size_y * size_y + size_z * size_z))+axis_y*((density*size_x*size_y*size_z) / 12 * (size_x * size_x + size_z * size_z))+axis_z*((density*size_x*size_y*size_z) / 12 * (size_x * size_x + size_y * size_y)))}" friction="${friction_scale*(axis_x*((density*size_x*size_y*size_z) / 12 * (size_y * size_y + size_z * size_z))+axis_y*((density*size_x*size_y*size_z) / 12 * (size_x * size_x + size_z * size_z))+axis_z*((density*size_x*size_y*size_z) / 12 * (size_x * size_x + size_y * size_y)))}"/>
  </xacro:macro>

  <!-- origin should point to the center of the sphere -->
  <xacro:macro name="sphere_inertial" params="density radius *origin">
    <inertial>
      <mass value="${(density*4.0*pi*radius*radius*radius/3.0)}"/>
      <insert_block name="origin"/>
      <inertia ixx="${2.0*(density*4.0*pi*radius*radius*radius/3.0)*radius*radius/5.0}" ixy="0.0" ixz="0.0" iyy="${2.0*(density*4.0*pi*radius*radius*radius/3.0)*radius*radius/5.0}" iyz="0.0" izz="${2.0*(density*4.0*pi*radius*radius*radius/3.0)*radius*radius/5.0}"/>
    </inertial>
  </xacro:macro>
  <xacro:macro name="sphere_limits" params="effort_scale density radius velocity_limit joint_lower joint_upper">
    <limit effort="${(density*4.0*pi*radius*radius*radius/3.0)*gravity*effort_scale*radius*2.0}" velocity="${velocity_limit}" lower="${joint_lower}" upper="${joint_upper}"/>
  </xacro:macro>
   <xacro:macro name="sphere_axis_dynamics" params="density radius axis_x axis_y axis_z damping_scale friction_scale">
    <axis xyz="${axis_x} ${axis_y} ${axis_z}"/>
    <dynamics damping="${damping_scale*(axis_x*(2.0*(density*4.0*pi*radius*radius*radius/3.0)*radius*radius/5.0)+axis_y*(2.0*(density*4.0*pi*radius*radius*radius/3.0)*radius*radius/5.0)+axis_z*(2.0*(density*4.0*pi*radius*radius*radius/3.0)*radius*radius/5.0))}" friction="${friction_scale*(axis_x*(2.0*(density*4.0*pi*radius*radius*radius/3.0)*radius*radius/5.0)+axis_y*(2.0*(density*4.0*pi*radius*radius*radius/3.0)*radius*radius/5.0)+axis_z*(2.0*(density*4.0*pi*radius*radius*radius/3.0)*radius*radius/5.0))}"/>
  </xacro:macro>

  <!-- origin should point to the center of the sphere -->
  <xacro:macro name="hollow_sphere_inertial" params="density inner_radius outer_radius *origin">
    <inertial>
      <mass value="${(density*4.0*pi*(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius)/3.0)}"/>
      <insert_block name="origin"/>
      <inertia ixx="${2.0*(density*4.0*pi*(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius)/3.0)*((outer_radius*outer_radius*outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius*inner_radius*inner_radius)/(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius))/5.0}" ixy="0.0" ixz="0.0" iyy="${2.0*(density*4.0*pi*(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius)/3.0)*((outer_radius*outer_radius*outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius*inner_radius*inner_radius)/(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius))/5.0}" iyz="0.0" izz="${2.0*(density*4.0*pi*(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius)/3.0)*((outer_radius*outer_radius*outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius*inner_radius*inner_radius)/(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius))/5.0}"/>
    </inertial>
  </xacro:macro>
  <xacro:macro name="hollow_sphere_limits" params="effort_scale density inner_radius outer_radius velocity_limit joint_lower joint_upper">
    <limit effort="${(density*4.0*pi*(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius)/3.0)*gravity*effort_scale*outer_radius*2.0}" velocity="${velocity_limit}" lower="${joint_lower}" upper="${joint_upper}"/>
  </xacro:macro>
   <xacro:macro name="hollow_sphere_axis_dynamics" params="density inner_radius outer_radius axis_x axis_y axis_z damping_scale friction_scale">
    <axis xyz="${axis_x} ${axis_y} ${axis_z}"/>
    <dynamics damping="${damping_scale*(axis_x*(2.0*(density*4.0*pi*(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius)/3.0)*((outer_radius*outer_radius*outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius*inner_radius*inner_radius)/(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius))/5.0)+axis_y*(2.0*(density*4.0*pi*(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius)/3.0)*((outer_radius*outer_radius*outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius*inner_radius*inner_radius)/(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius))/5.0)+axis_z*(2.0*(density*4.0*pi*(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius)/3.0)*((outer_radius*outer_radius*outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius*inner_radius*inner_radius)/(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius))/5.0))}" friction="${friction_scale*(axis_x*(2.0*(density*4.0*pi*(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius)/3.0)*((outer_radius*outer_radius*outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius*inner_radius*inner_radius)/(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius))/5.0)+axis_y*(2.0*(density*4.0*pi*(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius)/3.0)*((outer_radius*outer_radius*outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius*inner_radius*inner_radius)/(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius))/5.0)+axis_z*(2.0*(density*4.0*pi*(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius)/3.0)*((outer_radius*outer_radius*outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius*inner_radius*inner_radius)/(outer_radius*outer_radius*outer_radius-inner_radius*inner_radius*inner_radius))/5.0))}"/>
  </xacro:macro>
</robot>