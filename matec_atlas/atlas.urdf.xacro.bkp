<?xml version="1.0"?>
<robot name="atlas" xmlns:xacro="http://www.ros.org/wiki/xacro">
  <!-- Atlas's total mass is probably around 177.05Kg based on foot sensor force measurements-->
  <xacro:macro name="atlas" params="collisions sa_cameras">
    <link name="l_clav">
      <inertial>
        <mass value="3.45"/>
        <origin xyz="0 0.048 -0.084" rpy="0 -0 0"/>
        <inertia ixx="0.011" ixy="0" ixz="0" iyy="0.009" iyz="-0.004" izz="0.004"/>
      </inertial>
      <visual>
        <origin xyz="0 -0.044 -0.34" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/l_clav.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 -0.044 -0.34" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/l_clav.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="l_farm">
      <inertial>
        <mass value="3.388"/>
        <origin xyz="0 0.065 0" rpy="0 -0 0"/>
        <inertia ixx="0.00656" ixy="0" ixz="0" iyy="0.00358" iyz="0" izz="0.00656"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/l_farm.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/l_farm.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="l_foot">
      <inertial>
        <mass value="2.05"/>
        <origin xyz="0.038 0 -0.04" rpy="0 -0 0"/>
        <inertia ixx="0.002" ixy="0" ixz="0" iyy="0.007" iyz="0" izz="0.008"/>
      </inertial>
      <!-- <visual>
        <origin xyz="-0.008 0.002 0.024" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/l_foot.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
      <collision>
          <origin xyz="-0.008 0.002 0.024" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/l_foot.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if> -->
    </link>
    <link name="l_sole"/>
    <link name="l_foot_sensor"/>
    <link name="l_hand">
      <inertial>
        <mass value="2.509"/>
        <origin xyz="0 0.01 0" rpy="0 -0 0"/>
        <inertia ixx="0.00265" ixy="0" ixz="0" iyy="0.00446" iyz="0" izz="0.00446"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/l_hand.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/l_hand.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="l_end"/>
    <link name="l_wrist_sensor"/>
    <link name="l_larm">
      <inertial>
        <mass value="2.509"/>
        <origin xyz="0 0.075 0" rpy="0 -0 0"/>
        <inertia ixx="0.00265" ixy="0" ixz="0" iyy="0.00446" iyz="0" izz="0.00446"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/l_larm.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/l_larm.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="l_lglut">
      <inertial>
        <mass value="0.866"/>
        <origin xyz="0.0133341 0.0170484 -0.0312052" rpy="0 -0 0"/>
        <inertia ixx="0.000691326" ixy="-2.24344e-05" ixz="2.50508e-06" iyy="0.00126856" iyz="0.000137862" izz="0.00106487"/>
      </inertial>
      <visual>
        <origin xyz="0 0.005 -0.010" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/l_lglut.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_light_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0.005 -0.010" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/l_lglut.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="l_lleg">
      <inertial>
        <mass value="5.479"/>
        <origin xyz="0.001 0 -0.187" rpy="0 -0 0"/>
        <inertia ixx="0.077" ixy="0" ixz="-0.003" iyy="0.076" iyz="0" izz="0.01"/>
      </inertial>
      <visual>
        <origin xyz="0 0 -0.002" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/l_lleg.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 -0.002" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/l_lleg.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="l_scap">
      <inertial>
        <mass value="3.012"/>
        <origin xyz="0 0.075 0" rpy="0 -0 0"/>
        <inertia ixx="0.00319" ixy="0" ixz="0" iyy="0.00583" iyz="0" izz="0.00583"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/l_scap.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/l_scap.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="l_talus">
      <inertial>
        <mass value="0.125"/>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <inertia ixx="1.01674e-05" ixy="0" ixz="0" iyy="8.42775e-06" iyz="0" izz="1.30101e-05"/>
      </inertial>
      <visual>
        <origin xyz="0.014 0 0.008" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/l_talus.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_light_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0.014 0 0.008" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/l_talus.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="l_uarm">
      <inertial>
        <mass value="3.388"/>
        <origin xyz="0 0.065 0" rpy="0 -0 0"/>
        <inertia ixx="0.00656" ixy="0" ixz="0" iyy="0.00358" iyz="0" izz="0.00656"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/l_uarm.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/l_uarm.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="l_uglut">
      <inertial>
        <mass value="0.648"/>
        <origin xyz="0.00529262 -0.00344732 0.00313046" rpy="0 -0 0"/>
        <inertia ixx="0.00074276" ixy="-3.79607e-08" ixz="-2.79549e-05" iyy="0.000688179" iyz="-3.2735e-08" izz="0.00041242"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/l_uglut.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_light_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/l_uglut.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="l_uleg">
      <inertial>
        <mass value="9.209"/>
        <origin xyz="0 0 -0.21" rpy="0 -0 0"/>
        <inertia ixx="0.09" ixy="0" ixz="0" iyy="0.09" iyz="0" izz="0.02"/>
      </inertial>
      <visual>
        <origin xyz="-0.002 -0.015 0.006" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/l_uleg.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_light_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="-0.002 -0.015 0.006" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/l_uleg.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="ltorso">
      <inertial>
        <mass value="2.409"/>
        <origin xyz="-0.0112984 -3.15366e-06 0.0746835" rpy="0 -0 0"/>
        <inertia ixx="0.0039092" ixy="-5.04491e-08" ixz="-0.000342157" iyy="0.00341694" iyz="4.87119e-07" izz="0.00174492"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/ltorso.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/ltorso.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="mtorso">
      <inertial>
        <mass value="0.69"/>
        <origin xyz="-0.00816266 -0.0131245 0.0305974" rpy="0 -0 0"/>
        <inertia ixx="0.000454181" ixy="-6.10764e-05" ixz="3.94009e-05" iyy="0.000483282" iyz="5.27463e-05" izz="0.000444215"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/mtorso.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/mtorso.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="pelvis">
      <inertial>
        <mass value="17.882"/>
        <origin xyz="0.0111 0 0.0271" rpy="0 -0 0"/>
        <inertia ixx="0.1244" ixy="0.0008" ixz="-0.0007" iyy="0.0958" iyz="-0.0005" izz="0.1167"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/pelvis.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_light_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/pelvis.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="imu"/>
    <link name="r_clav">
      <inertial>
        <mass value="3.45"/>
        <origin xyz="0 -0.048 -0.084" rpy="0 -0 0"/>
        <inertia ixx="0.011" ixy="0" ixz="0" iyy="0.009" iyz="0.004" izz="0.004"/>
      </inertial>
      <visual>
        <origin xyz="0 0.044 -0.34" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/r_clav.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0.044 -0.34" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/r_clav.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="r_farm">
      <inertial>
        <mass value="3.388"/>
        <origin xyz="0 -0.065 0" rpy="0 -0 0"/>
        <inertia ixx="0.00656" ixy="0" ixz="0" iyy="0.00358" iyz="0" izz="0.00656"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/r_farm.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/r_farm.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="r_foot">
      <inertial>
        <mass value="2.05"/>
        <origin xyz="0.038 0 -0.04" rpy="0 -0 0"/>
        <inertia ixx="0.002" ixy="0" ixz="0" iyy="0.007" iyz="0" izz="0.008"/>
      </inertial>
      <!-- <visual>
        <origin xyz="-0.008 -0.002 0.024" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/r_foot.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
      <collision>
          <origin xyz="-0.008 -0.002 0.024" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/r_foot.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if> -->
    </link>
    <link name="r_sole"/>
    <link name="r_foot_sensor"/>
    <link name="r_hand">
      <inertial>
        <mass value="2.509"/>
        <origin xyz="0 -0.01 0" rpy="0 -0 0"/>
        <inertia ixx="0.00265" ixy="0" ixz="0" iyy="0.00446" iyz="0" izz="0.00446"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/r_hand.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/r_hand.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>  
    <link name="r_end"/>
    <link name="r_wrist_sensor"/>
    <link name="r_larm">
      <inertial>
        <mass value="2.509"/>
        <origin xyz="0 -0.075 0" rpy="0 -0 0"/>
        <inertia ixx="0.00265" ixy="0" ixz="0" iyy="0.00446" iyz="0" izz="0.00446"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/r_larm.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/r_larm.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="r_lglut">
      <inertial>
        <mass value="0.866"/>
        <origin xyz="0.0133341 -0.0170484 -0.0312052" rpy="0 -0 0"/>
        <inertia ixx="0.000691326" ixy="2.24344e-05" ixz="2.50508e-06" iyy="0.00126856" iyz="-0.000137862" izz="0.00106487"/>
      </inertial>
      <visual>
        <origin xyz="0.010 -0.005 -0.000" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/r_lglut.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_light_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0.010 -0.005 -0.000" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/r_lglut.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="r_lleg">
      <inertial>
        <mass value="5.479"/>
        <origin xyz="0.001 0 -0.187" rpy="0 -0 0"/>
        <inertia ixx="0.077" ixy="-0" ixz="-0.003" iyy="0.076" iyz="-0" izz="0.01"/>
      </inertial>
      <visual>
        <origin xyz="0 0 -0.002" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/r_lleg.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 -0.002" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/r_lleg.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="r_scap">
      <inertial>
        <mass value="3.012"/>
        <origin xyz="0 -0.075 0" rpy="0 -0 0"/>
        <inertia ixx="0.00319" ixy="0" ixz="0" iyy="0.00583" iyz="0" izz="0.00583"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/r_scap.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/r_scap.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="r_talus">
      <inertial>
        <mass value="0.125"/>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <inertia ixx="1.01674e-05" ixy="0" ixz="0" iyy="8.42775e-06" iyz="0" izz="1.30101e-05"/>
      </inertial>
      <visual>
        <origin xyz="0.014 0 0.008" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/r_talus.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_light_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0.014 0 0.008" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/r_talus.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="r_uarm">
      <inertial>
        <mass value="3.388"/>
        <origin xyz="0 -0.065 0" rpy="0 -0 0"/>
        <inertia ixx="0.00656" ixy="0" ixz="0" iyy="0.00358" iyz="0" izz="0.00656"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/r_uarm.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_dark_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/r_uarm.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="r_uglut">
      <inertial>
        <mass value="0.648"/>
        <origin xyz="0.00529262 0.00344732 0.00313046" rpy="0 -0 0"/>
        <inertia ixx="0.00074276" ixy="3.79607e-08" ixz="-2.79549e-05" iyy="0.000688179" iyz="3.2735e-08" izz="0.00041242"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/r_uglut.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_light_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/r_uglut.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="r_uleg">
      <inertial>
        <mass value="9.209"/>
        <origin xyz="0 0 -0.21" rpy="0 -0 0"/>
        <inertia ixx="0.09" ixy="0" ixz="0" iyy="0.09" iyz="0" izz="0.02"/>
      </inertial>
      <visual>
        <origin xyz="-0.002 0.015 0.006" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/r_uleg.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_light_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="-0.002 0.015 0.006" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/r_uleg.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <!-- with backpack -->
    <link name="utorso">
      <inertial>
        <mass value="63.73"/>
        <origin xyz="-0.0581 0 0.3056" rpy="0 -0 0"/>
        <inertia ixx="1.466" ixy="0.00362" ixz="0.336" iyy="1.51" iyz="0.001" izz="1.3"/>
      </inertial>
      <visual>
        <origin xyz="0 0 0" rpy="0 -0 0"/>
        <geometry>
          <mesh filename="package://matec_atlas/meshes/utorso.stl" scale="1 1 1"/>
        </geometry>
        <material name="matec_light_gray"/>
      </visual>
      <xacro:if value="${collisions}">
        <collision>
          <origin xyz="0 0 0" rpy="0 -0 0"/>
          <geometry>
            <mesh filename="package://matec_atlas/meshes/utorso.stl" scale="1 1 1"/>
          </geometry>
        </collision>
      </xacro:if>
    </link>
    <link name="head_root"/>

    <!-- left arm -->
    <joint name="l_arm_shz" type="revolute">
      <!-- z is to the flat part at the top of the mouting bracket, or where the silver part at the top of the motor meets the black -->
      <!--my questionable measurements: <origin xyz="0.141 0.2025 0.476" rpy="0 -0 0"/> -->
      <origin xyz="0.1406 0.2036 0.4776" rpy="0 -0 0"/>
      <axis xyz="0 0 1"/>
      <parent link="utorso"/>
      <child link="l_clav"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -198, f_max = 198 -->
      <limit effort="198" velocity="1" lower="-1.5677281767" upper="0.79604016096"/>
    </joint>
    <joint name="l_arm_shx" type="revolute">
      <origin xyz="0 0.115 -0.2445" rpy="0 -0 0"/>
      <!-- <origin xyz="0 0.0981 -0.2511" rpy="0 -0 0"/> -->
      <axis xyz="1 0 0"/>
      <parent link="l_clav"/>
      <child link="l_scap"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -159, f_max = 159 -->
      <limit effort="159" velocity="1" lower="-1.570796327" upper="1.570796327"/>
    </joint>
    <joint name="l_arm_ely" type="revolute">
      <!-- roll joints measured to the part farther down the chain -->
      <origin xyz="0 0.176 -0.0175" rpy="0 -0 0"/>
      <!-- <origin xyz="0 0.187 0.016" rpy="0 -0 0"/> -->
      <axis xyz="0 1 0"/>
      <parent link="l_scap"/>
      <child link="l_uarm"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -106, f_max = 106 -->
      <limit effort="106" velocity="1" lower="0" upper="3.141592654"/>
    </joint>
    <joint name="l_arm_elx" type="revolute">
      <origin xyz="0 0.132 0.006" rpy="0 -0 0"/>
      <!-- <origin xyz="0 0.119 0.0092" rpy="0 -0 0"/> -->
      <axis xyz="1 0 0"/>
      <parent link="l_uarm"/>
      <child link="l_larm"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -159, f_max = 159 -->
      <limit effort="159" velocity="1" lower="0" upper="2.35619449"/>
    </joint>
    <joint name="l_arm_uwy" type="revolute">
      <origin xyz="0 0.176 -0.0175" rpy="0 -0 0"/>
      <axis xyz="0 1 0"/>
      <parent link="l_larm"/>
      <child link="l_farm"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -106, f_max = 106 -->
      <limit effort="106" velocity="1" lower="0" upper="3.141592654"/>
    </joint>
    <joint name="l_arm_mwx" type="revolute">
      <origin xyz="0 0.132 0.006" rpy="0 -0 0"/>
      <axis xyz="1 0 0"/>
      <parent link="l_farm"/>
      <child link="l_hand"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -56, f_max = 56 -->
      <limit effort="56" velocity="1" lower="-1.178097245" upper="1.178097245"/>
    </joint>
    <joint name="l_end_joint" type="fixed">
      <origin xyz="0.005 0.104 -0.0112" rpy="0.0 0.0 1.57"/>
      <parent link="l_hand"/>
      <child link="l_end"/>
    </joint>
    <joint name="l_wrist_sensor_joint" type="fixed">
      <origin xyz="0.0 0.1245 -0.0112" rpy="1.5707963267948968 -1.5707963267948963 0.0"/>
      <parent link="l_hand"/>
      <child link="l_wrist_sensor"/>
    </joint>

    <!-- right arm -->
    <joint name="r_arm_shz" type="revolute">
      <!-- z is to the flat part at the top of the mouting bracket, or where the silver part at the top of the motor meets the black -->
      <origin xyz="0.1406 -0.2036 0.4776" rpy="0 -0 0"/>
      <axis xyz="0 0 1"/> 
      <parent link="utorso"/>
      <child link="r_clav"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -198, f_max = 198 -->
      <limit effort="198" velocity="1" lower="-0.81454399" upper="1.56648167981"/>
    </joint>
    <joint name="r_arm_shx" type="revolute">
      <origin xyz="0 -0.115 -0.2445" rpy="0 -0 0"/>
      <!-- <origin xyz="0 -0.0981 -0.2511" rpy="0 -0 0"/> -->
      <axis xyz="1 0 0"/>
      <parent link="r_clav"/>
      <child link="r_scap"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -159, f_max = 159 -->
      <limit effort="159" velocity="1" lower="-1.570796327" upper="1.570796327"/>
    </joint>
    <joint name="r_arm_ely" type="revolute">
      <origin xyz="0 -0.176 -0.0175" rpy="0 -0 0"/>
      <axis xyz="0 1 0"/>
      <parent link="r_scap"/>
      <child link="r_uarm"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -106, f_max = 106 -->
      <limit effort="106" velocity="1" lower="0" upper="3.141592654"/>
    </joint>
    <joint name="r_arm_elx" type="revolute">
      <origin xyz="0 -0.132 0.006" rpy="0 -0 0"/>
      <axis xyz="1 0 0"/>
      <parent link="r_uarm"/>
      <child link="r_larm"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -159, f_max = 159 -->
      <limit effort="159" velocity="1" lower="-2.35619449" upper="0"/>
    </joint>
    <joint name="r_arm_uwy" type="revolute">
      <origin xyz="0 -0.176 -0.0175" rpy="0 -0 0"/>
      <axis xyz="0 1 0"/>
      <parent link="r_larm"/>
      <child link="r_farm"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -106, f_max = 106 -->
      <limit effort="106" velocity="1" lower="0" upper="3.141592654"/>
    </joint>
    <joint name="r_arm_mwx" type="revolute">
      <origin xyz="0 -0.132 0.006" rpy="0 -0 0"/>
      <axis xyz="1 0 0"/>
      <parent link="r_farm"/>
      <child link="r_hand"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -56, f_max = 56 -->
      <limit effort="56" velocity="1" lower="-1.178097245" upper="1.178097245"/>
    </joint>
    <joint name="r_end_joint" type="fixed">
      <origin xyz="-0.005 -0.104 0.0112" rpy="0.0 0.0 -1.57"/>
      <parent link="r_hand"/>
      <child link="r_end"/>
    </joint>
    <joint name="r_wrist_sensor_joint" type="fixed">
      <origin xyz="0.0 -0.1245 0.0112" rpy="-1.5707963267948968 1.5707963267948963 0.0"/>
      <parent link="r_hand"/>
      <child link="r_wrist_sensor"/>
    </joint>

    <!-- neck -->
    <joint name="neck_ay" type="revolute">
      <!-- <origin xyz="0.2546 0 0.6215" rpy="0 -0 0"/> -->
      <origin xyz="0.2705 0 0.6165" rpy="0 -0 0"/>
      <axis xyz="0 1 0"/>
      <parent link="utorso"/>
      <child link="head_root"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -5, f_max = 5 -->
      <limit effort="5" velocity="1" lower="-0.602138592" upper="1.14319066"/>
    </joint>

    <!-- back -->
    <joint name="back_bkx" type="revolute">
      <origin xyz="0 0 0.05" rpy="0 -0 0"/>
      <axis xyz="1 0 0"/>
      <parent link="mtorso"/>
      <child link="utorso"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -163, f_max = 163 -->
      <limit effort="163" velocity="1" lower="-0.523598776" upper="0.523598776"/>
    </joint>
    <joint name="back_bky" type="revolute">
      <origin xyz="0 0 0.162" rpy="0 -0 0"/>
      <axis xyz="0 1 0"/>
      <parent link="ltorso"/>
      <child link="mtorso"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -245, f_max = 245 -->
      <limit effort="245" velocity="9" lower="-0.219387887" upper="0.53878314"/>
    </joint>
    <joint name="back_bkz" type="revolute">
      <!-- BDI specified the joint at the level of the pelvis plate, not where the rotating contact appears to be (so no z offset here). back_bky has a larger offset to compensate -->
      <origin xyz="-0.0125 0 0" rpy="0 -0 0"/>
      <axis xyz="0 0 1"/>
      <parent link="pelvis"/>
      <child link="ltorso"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -62, f_max = 62 -->
      <limit effort="62" velocity="1" lower="-0.663225116" upper="0.663225116"/>
    </joint>
    <joint name="imu_joint" type="fixed">
      <origin xyz="-0.0905 -0.000004 -0.0125" rpy="0.0 0.0 0.7854131108799551"/>
      <parent link="pelvis"/>
      <child link="imu"/>
    </joint>

    <!-- left leg -->
    <joint name="l_leg_hpz" type="revolute">
      <origin xyz="0 0.0895 0" rpy="0 -0 0"/>
      <axis xyz="0 0 1"/>
      <parent link="pelvis"/>
      <child link="l_uglut"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -150, f_max = 150 -->
      <limit effort="150" velocity="1" lower="-0.174358392" upper="0.786794427"/>
    </joint>
    <joint name="l_leg_hpx" type="revolute">
      <origin xyz="0 0 0" rpy="0 -0 0"/>
      <axis xyz="1 0 0"/>
      <parent link="l_uglut"/>
      <child link="l_lglut"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -257, f_max = 257 -->
      <limit effort="257" velocity="1" lower="-0.523598776" upper="0.523598776"/>
    </joint>
    <joint name="l_leg_hpy" type="revolute">
      <origin xyz="0.05 0.0225 -0.066" rpy="0 -0 0"/>
      <axis xyz="0 1 0"/>
      <parent link="l_lglut"/>
      <child link="l_uleg"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- SKEW: f_min = -723, f_max = 400 -->
      <limit effort="723" velocity="1" lower="-1.612335163" upper="0.657640062"/>
    </joint>
    <joint name="l_leg_kny" type="revolute">
      <origin xyz="-0.05 0 -0.3765" rpy="0 -0 0"/>
      <axis xyz="0 1 0"/>
      <parent link="l_uleg"/>
      <child link="l_lleg"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- SKEW: f_min = -446, f_max = 246 -->
      <limit effort="446" velocity="1" lower="0" upper="2.356369023"/>
    </joint>
    <joint name="l_leg_aky" type="revolute">
      <origin xyz="0 0 -0.422" rpy="0 -0 0"/>
      <axis xyz="0 1 0"/>
      <parent link="l_lleg"/>
      <child link="l_talus"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- SKEW: f_min = -110, f_max = 350 -->
      <limit effort="350" velocity="1" lower="-1" upper="0.7"/>
    </joint>
    <joint name="l_leg_akx" type="revolute">
      <origin xyz="0 0 0" rpy="0 -0 0"/>
      <axis xyz="1 0 0"/>
      <parent link="l_talus"/>
      <child link="l_foot"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -45, f_max = 45 -->
      <limit effort="45" velocity="1" lower="-0.8" upper="0.8"/>
    </joint>
    <joint name="l_sole_joint" type="fixed">
      <origin xyz="0.038 0 -0.08" rpy="0 -0 0"/>
      <parent link="l_foot"/>
      <child link="l_sole"/>
    </joint>
    <joint name="l_foot_sensor_joint" type="fixed">
      <!--should possibly have an x offset as well? -->
      <origin xyz="0.0 0.0 -0.039" rpy="0.0 0.0 0.0"/>
      <parent link="l_foot"/>
      <child link="l_foot_sensor"/>
    </joint>

    <!-- right leg -->
    <joint name="r_leg_hpz" type="revolute">
      <origin xyz="0 -0.0895 0" rpy="0 -0 0"/>
      <axis xyz="0 0 1"/>
      <parent link="pelvis"/>
      <child link="r_uglut"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -150, f_max = 150 -->
      <limit effort="150" velocity="1" lower="-0.786794427" upper="0.174358392"/>
    </joint>
    <joint name="r_leg_hpx" type="revolute">
      <origin xyz="0 0 0" rpy="0 -0 0"/>
      <axis xyz="1 0 0"/>
      <parent link="r_uglut"/>
      <child link="r_lglut"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -257, f_max = 257 -->
      <limit effort="257" velocity="1" lower="-0.523598776" upper="0.523598776"/>
    </joint>
    <joint name="r_leg_hpy" type="revolute">
      <origin xyz="0.05 -0.0225 -0.066" rpy="0 -0 0"/>
      <axis xyz="0 1 0"/>
      <parent link="r_lglut"/>
      <child link="r_uleg"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- SKEW: f_min = -723, f_max = 400 -->
      <limit effort="723" velocity="1" lower="-1.612335163" upper="0.657640062"/>
    </joint>
    <joint name="r_leg_kny" type="revolute">
      <origin xyz="-0.05 0 -0.3765" rpy="0 -0 0"/>
      <axis xyz="0 1 0"/>
      <parent link="r_uleg"/>
      <child link="r_lleg"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- SKEW: f_min = -446, f_max = 246 -->
      <limit effort="446" velocity="1" lower="0" upper="2.356369023"/>
    </joint>
    <joint name="r_leg_aky" type="revolute">
      <origin xyz="0 0 -0.422" rpy="0 -0 0"/>
      <axis xyz="0 1 0"/>
      <parent link="r_lleg"/>
      <child link="r_talus"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- SKEW: f_min = -110, f_max = 350 -->
      <limit effort="350" velocity="1" lower="-1" upper="0.7"/>
    </joint>
    <joint name="r_leg_akx" type="revolute">
      <origin xyz="0 0 0" rpy="0 -0 0"/>
      <axis xyz="1 0 0"/>
      <parent link="r_talus"/>
      <child link="r_foot"/>
      <dynamics damping="0.6" friction="0"/>
      <!-- f_min = -45, f_max = 45 -->
      <limit effort="45" velocity="1" lower="-0.8" upper="0.8"/>
    </joint>
    <joint name="r_sole_joint" type="fixed">
      <origin xyz="0.038 0 -0.08" rpy="0 -0 0"/>
      <parent link="r_foot"/>
      <child link="r_sole"/>
    </joint>
    <joint name="r_foot_sensor_joint" type="fixed">
      <!--should possibly have an x offset as well? -->
      <origin xyz="0.0 0.0 -0.039" rpy="0.0 0.0 0.0"/>
      <parent link="r_foot"/>
      <child link="r_foot_sensor"/>
    </joint>

    <xacro:if value="${sa_cameras}">
      <!-- new situational awareness cameras -->
      <link name="r_situational_awareness_camera_link">
        <inertial>
          <mass value="0.1"/>
          <origin xyz="0 0 0"/>
          <inertia ixx="0.00001" ixy="0" ixz="0" iyy="0.00001" iyz="0" izz="0.00001"/>
        </inertial>
        <visual>
          <origin rpy="0 0 0" xyz="0 0 0"/>
          <geometry>
            <box size="0.01 0.01 0.01"/>
          </geometry>
          <material name="matec_dark_gray"/>
        </visual>
      </link>
      <joint name="r_situational_awareness_camera_joint" type="fixed">
        <parent link="utorso"/>
        <child link="r_situational_awareness_camera_link"/>
        <!-- if camera points x-forward, bottom towards front of robot, 15 degree yaw forwards from
             lateral (+y) view -->
        <origin xyz="0.216409 -0.121799 0.406759" rpy="1.57079633 0 -1.30899694"/>
      </joint>
      <joint name="r_situational_awareness_camera_optical_frame_joint" type="fixed">
        <origin xyz="0 0 0" rpy="-1.570796327 0.0 -1.570796327"/>
        <parent link="r_situational_awareness_camera_link"/>
        <child link="r_situational_awareness_camera_optical_frame"/>
      </joint>
      <link name="r_situational_awareness_camera_optical_frame"/>

      <link name="l_situational_awareness_camera_link">
        <inertial>
          <mass value="0.1"/>
          <origin xyz="0 0 0"/>
          <inertia ixx="0.00001" ixy="0" ixz="0" iyy="0.00001" iyz="0" izz="0.00001"/>
        </inertial>
        <visual>
          <origin rpy="0 0 0" xyz="0 0 0"/>
          <geometry>
            <box size="0.01 0.01 0.01"/>
          </geometry>
          <material name="matec_dark_gray"/>
        </visual>
      </link>
      <joint name="l_situational_awareness_camera_joint" type="fixed">
        <parent link="utorso"/>
        <child link="l_situational_awareness_camera_link"/>
        <!-- if camera points x-forward, bottom towards front of robot, 15 degree yaw forwards from
             lateral (+y) view -->
        <origin xyz="0.216409 0.121799 0.406759" rpy="-1.57079633 0 1.30899694"/>
      </joint>
      <joint name="l_situational_awareness_camera_optical_frame_joint" type="fixed">
        <origin xyz="0 0 0" rpy="-1.570796327 0.0 -1.570796327"/>
        <parent link="l_situational_awareness_camera_link"/>
        <child link="l_situational_awareness_camera_optical_frame"/>
      </joint>
      <link name="l_situational_awareness_camera_optical_frame"/>

      <link name="rear_situational_awareness_camera_link">
        <inertial>
          <mass value="0.1"/>
          <origin xyz="0 0 0"/>
          <inertia ixx="0.00001" ixy="0" ixz="0" iyy="0.00001" iyz="0" izz="0.00001"/>
        </inertial>
        <visual>
          <origin rpy="0 0 0" xyz="0 0 0"/>
          <geometry>
            <box size="0.01 0.01 0.01"/>
          </geometry>
          <material name="matec_dark_gray"/>
        </visual>
      </link>
      <joint name="rear_situational_awareness_camera_joint" type="fixed">
        <parent link="utorso"/>
        <child link="rear_situational_awareness_camera_link"/>
        <!-- if camera points towards -X direction of the utorso link frame -->
        <origin xyz="-0.0026 0.0264 0.6649" rpy="0 0 3.141592654"/>
      </joint>
      <joint name="rear_situational_awareness_camera_optical_frame_joint" type="fixed">
        <origin xyz="0 0 0" rpy="-1.570796327 0.0 -1.570796327"/>
        <parent link="rear_situational_awareness_camera_link"/>
        <child link="rear_situational_awareness_camera_optical_frame"/>
      </joint>
      <link name="rear_situational_awareness_camera_optical_frame"/>
    </xacro:if>
  </xacro:macro>
</robot>
